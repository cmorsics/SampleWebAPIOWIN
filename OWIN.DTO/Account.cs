﻿namespace Sample.WebAPI.OWIN.DTO
{
    public class Account
    {
        public int Id { get; set; }
        public decimal AmmountDue { get; set; }
    }
}
