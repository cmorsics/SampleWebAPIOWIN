using System;
using System.Collections.Generic;

namespace Sample.WebAPI.OWIN.DTO
{
	public interface IBusinessLogic : IDisposable
	{
	    Result GetSampleResult(string parameter1, string parameter2);
        int SaveCustomer(Customer customer);
        int SaveAccount(int customerId, Account account);
        IList<Account> GetAccounts(int customerId);
        Customer GetCustomer(int customerId);
	}
}