﻿using System;

namespace Sample.WebAPI.OWIN.DTO
{
    public class Result
    {
        public string Message { get; set; }

        public Exception Exception { get; set; }
    }
}
