﻿using System.Collections.Generic;

namespace Sample.WebAPI.OWIN.DTO
{
    public class Customer
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public virtual ICollection<Account> Accounts { get; set; }

        public Customer()
        {
           Accounts = new List<Account>(); 
        }
    }
}
