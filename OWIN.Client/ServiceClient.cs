﻿using System;
using RestSharp;

namespace Sample.WebAPI.OWIN.Client
{
    // TODO: generate this using swagger-codegen
    public class ServiceClient : IDisposable
    {
        private readonly RestClient _client;
        private readonly string _applicationName;

        public static string Version { get { return "1.0"; } }

        public ServiceClient(string applicationName)
        {
            _client = new RestClient("http://localhost:53204/api/sampleowin/") { Authenticator = new NtlmAuthenticator() };
            _applicationName = applicationName;
        }

        public void Dispose()
        {
        }

        public bool IsClientCurrent(string version)
        {
            var request = new RestRequest("isclientcurrent/{version}", Method.GET) { RequestFormat = DataFormat.Json };
            request.AddUrlSegment("version", version);

            // easily add HTTP Headers
            request.AddHeader("application", _applicationName);
            var response = _client.Execute<bool>(request);

            return response.Data;
        }

    }
}
