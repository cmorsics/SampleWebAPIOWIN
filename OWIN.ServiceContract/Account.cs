﻿namespace Sample.WebAPI.OWIN.ServiceContract
{
    public abstract class Account
    {
        int Id { get; set; }
        decimal AmmountDue { get; set; }
    }
}
