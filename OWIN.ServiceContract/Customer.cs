﻿using System.Collections.Generic;

namespace Sample.WebAPI.OWIN.ServiceContract
{
    public abstract class Customer
    {
        int Id { get; set; }
        string FirstName { get; set; }
        string LastName { get; set; }

        IList<Account> Accounts { get; set; }
    }
}
