﻿using System;

namespace Sample.WebAPI.OWIN.ServiceContract
{
    public class Result
    {
        public string Message { get; set; }

        public Exception Exception { get; set; }
    }
}
