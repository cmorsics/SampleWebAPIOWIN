﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Sample.WebAPI.OWIN.Client;

namespace Sample.WebAPI.OWIN.UnitTest
{
    [TestClass]
    public class ClientTest
    {
        [TestMethod]
        public void IsClientCurrent()
        {
            using (var client = new ServiceClient("Client Test"))
            {
                var result = client.IsClientCurrent("1.0");

                Assert.AreEqual(true, result);
            }
        }

    }
}
