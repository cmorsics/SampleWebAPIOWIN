﻿using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace Sample.WebAPI.OWIN.WebAPI.Filters
{
    public class LoggingFilter : ActionFilterAttribute
    {
        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            //var request = actionContext.Request;
            //request.GetCorrelationId().ToString();
            //request.Method.ToString();
            //request.RequestUri.PathAndQuery;

            //log("Begin method call")
        }

        public override void OnActionExecuted(HttpActionExecutedContext actionExecutedContext)
        {
            //if (actionExecutedContext.Exception != null)
                //log("Uncaught exception")
        }
    }
}