﻿using System;
using System.Threading;
using System.Web.Http;
using Microsoft.Owin;
using Microsoft.Owin.BuilderProperties;
using Microsoft.Owin.Extensions;
using Microsoft.Owin.Hosting;
using Owin;
using Sample.WebAPI.OWIN.WebAPI;

[assembly: OwinStartup(typeof(Startup))]
namespace Sample.WebAPI.OWIN.WebAPI
{
    public class Startup
    {
        //Used for self hosting the service, this does not affect IIS Startup
        public static void StartServer()
        {
            const string baseAddress = "http://localhost:8081/";
            var startup = new Startup();

            using (WebApp.Start(baseAddress, startup.Configuration))
            {
                Console.WriteLine("Started...");

                Console.ReadKey();
            }            
        }
           
        // This code configures Web API. The Startup class is specified as a type
        // parameter in the WebApp.Start method.
        public void Configuration(IAppBuilder appBuilder) 
        { 
            var config = new HttpConfiguration();

            WebApiConfig.Register(config);

            appBuilder.UseWebApi(config);
            appBuilder.UseStageMarker(PipelineStage.MapHandler);

            // cleanup unity
            var shutdownToken = new AppProperties(appBuilder.Properties).OnAppDisposing;
            if (shutdownToken != CancellationToken.None)
            {
                shutdownToken.Register(() => UnityConfig.GetConfiguredContainer().Dispose());
            }
        }    
    }
}
