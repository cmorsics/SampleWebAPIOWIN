﻿using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.ModelBinding;
using Sample.WebAPI.OWIN.Client;
using Sample.WebAPI.OWIN.DTO;

namespace Sample.WebAPI.OWIN.WebAPI.Controllers
{
    /// <summary>
    /// Implement the WebAPI class
    /// </summary>
    public class SampleController : ApiController
    {
        private readonly IBusinessLogic _businessLogic;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="businessLogic"></param>
        public SampleController(IBusinessLogic businessLogic)
        {
            _businessLogic = businessLogic;
        }

        /// <summary>
        /// Checks if the client version is the latest
        /// </summary>
        /// <param name="version">Version to check</param>
        /// <returns></returns>
        [HttpGet]
        [Route("isclientcurrent/{version}")]
        public bool IsClientCurrent(string version)
        {
            return version == ServiceClient.Version;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="parameter1"></param>
        /// <param name="parameter2"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetSampleResult/{parameter1}/{parameter2}")]
        [ResponseType(typeof(Result))]
        public IHttpActionResult GetSampleResult(string parameter1, string parameter2)
        {
            var result = _businessLogic.GetSampleResult(parameter1, parameter2);
            if (result.Exception != null)
            {
                return BadRequest(result.Exception.Message);
            }
            return Ok(result);
        }

        [HttpGet]
        [Route("GetCustomer/{customerId}")]
        public IHttpActionResult GetCustomer(int customerId)
        {
            return Ok(_businessLogic.GetCustomer(customerId));
        }

        [HttpPost]
        [Route("SaveCustomer/")]
        public IHttpActionResult SaveCustomer([FromBody] Customer customer)
        {
            return Ok(_businessLogic.SaveCustomer(customer));
        }

        [HttpPost]
        [Route("SaveAccount/{customerId}")]
        public IHttpActionResult SaveAccount(int customerId, [FromBody] Account account)
        {
            return Ok(_businessLogic.SaveAccount(customerId, account));
        }

        [HttpGet]
        [Route("GetAccounts/{customerId}")]
        public IHttpActionResult GetAccounts(int customerId)
        {
            return Ok(_businessLogic.GetAccounts(customerId));
        }
    }
}