using System;
using System.Linq;
using Microsoft.Practices.Unity;

namespace Sample.WebAPI.OWIN.WebAPI
{
    /// <summary>
    /// Specifies the Unity configuration for the main container.
    /// </summary>
    public class UnityConfig
    {
        #region Unity Container
        private static readonly Lazy<IUnityContainer> container = new Lazy<IUnityContainer>(() =>
        {
            var container = new UnityContainer();
            RegisterTypes(container);
            return container;
        });

        /// <summary>
        /// Gets the configured Unity container.
        /// </summary>
        public static IUnityContainer GetConfiguredContainer()
        {
            return container.Value;
        }
        #endregion
        
        /// <summary>Registers the type mappings with the Unity container.</summary>
        /// <param name="container">The unity container to configure.</param>
        /// <remarks>There is no need to register concrete types such as controllers or API controllers (unless you want to 
        /// change the defaults), as Unity allows resolving a concrete type even if it was not previously registered.</remarks>
        public static void RegisterTypes(IUnityContainer container)
        {
            // NOTE: To load from web.config uncomment the line below. Make sure to add a Microsoft.Practices.Unity.Configuration to the using statements.
            // container.LoadConfiguration();

            var myAssemblies = AppDomain.CurrentDomain
               .GetAssemblies()
               .Where(a => a.FullName.StartsWith("OWIN.BusinessLogic") ||
                      a.FullName.StartsWith("WebAPI.Controllers") ||
                      a.FullName.StartsWith("WebAPI.OWIN.DTO") ||
                      a.FullName.StartsWith("WebAPI.OWIN.ServiceContract"));
            
            container.RegisterTypes(
                myAssemblies.SelectMany(a => a.GetTypes()),
                WithMappings.FromMatchingInterface,
                WithName.Default,
                WithLifetime.Transient);

        }

    }
}
