﻿using System;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Web.Http;
using System.Web.Http.Cors;
using Microsoft.Practices.Unity.WebApi;
using Newtonsoft.Json;
using Swashbuckle.Application;

namespace Sample.WebAPI.OWIN.WebAPI
{
    /// <summary>
    /// Configure the web app
    /// </summary>
    public static class WebApiConfig
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="config"></param>
        public static void Register(HttpConfiguration config)
        {
            // allow ajax from anywhere
            //var cors = new EnableCorsAttribute("http://localhost:57000", "*", "*")
            //{
            //    SupportsCredentials = true
            //};
            //config.EnableCors(cors);

            config.Formatters.Clear();
            config.Formatters.Add(new JsonMediaTypeFormatter());
            config.Formatters.JsonFormatter.SerializerSettings.TypeNameHandling = TypeNameHandling.Auto;

            //Authorization Filter
            //config.Filters.Add(new AuthorizeAttribute());

            // Attribute routing.
            config.MapHttpAttributeRoutes();

            // Convention-based routing.
            //config.Routes.MapHttpRoute(
            //    name: "DefaultApi",
            //    routeTemplate: "api",
            //    defaults: new { id = RouteParameter.Optional }
            //);

            config.DependencyResolver = new UnityDependencyResolver(UnityConfig.GetConfiguredContainer());
            
            config.EnableSwagger(c =>
                {
                    c.RootUrl(req => req.RequestUri.GetLeftPart(UriPartial.Authority) + req.GetRequestContext().VirtualPathRoot.TrimEnd('/'));
                    c.SingleApiVersion("v1", "OWIN.WebAPI");
                    c.IncludeXmlComments(GetXmlCommentsPath());
                    c.IgnoreObsoleteActions();
                    c.GroupActionsBy(apiDesc => apiDesc.HttpMethod.ToString());
                })
            .EnableSwaggerUi();
        }

        private static string GetXmlCommentsPath()
        {
            return string.Format(@"{0}bin\Content\OWIN.WebAPI.XML", AppDomain.CurrentDomain.BaseDirectory);
        }
    }
}