﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Sample.WebAPI.OWIN.BusinessLogic.Model;
using Sample.WebAPI.OWIN.DTO;

namespace Sample.WebAPI.OWIN.BusinessLogic
{
    public class BusinessLogic : IBusinessLogic
    {
        #region Private Properties

        private bool _disposed = false;
        public void Dispose()
        {
            if (!_disposed)
            {
                _disposed = true;
            }
        }

        public Result GetSampleResult(string parameter1, string parameter2)
        {
            return new Result();
        }

        public int SaveCustomer(Customer customer)
        {
            try
            {
                using (var db = new CustomerContext())
                {
                    db.Customers.Add(customer);
                    db.SaveChanges();

                    return customer.Id;
                }
            }
            catch (Exception e)
            {
                
                throw;
            }
        }

        public int SaveAccount(int customerId, Account account)
        {
            using (var db = new CustomerContext())
            {
                var customer = (from cust in db.Customers
                    where cust.Id == customerId
                    select cust).Single();

                customer.Accounts.Add(account);
                db.SaveChanges();
            }

            return account.Id;
        }

        public IList<Account> GetAccounts(int customerId)
        {
            using (var db = new CustomerContext())
            {
                var query = from cust in db.Customers
                    where cust.Id == customerId
                    select cust;

                var result = query
                    .SelectMany(c => c.Accounts)
                    .ToList();

                return result;
            }
        }

        public Customer GetCustomer(int customerId)
        {
            using (var db = new CustomerContext())
            {
                var query = from cust in db.Customers
                            where cust.Id == customerId
                            select cust;

                query = query.Include(cust => cust.Accounts);

                return query.Single();
            }
        }

        #endregion

    }
}