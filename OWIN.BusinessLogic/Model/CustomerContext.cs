﻿using System.Data.Entity;
using Sample.WebAPI.OWIN.DTO;

namespace Sample.WebAPI.OWIN.BusinessLogic.Model
{
    public class CustomerContext : DbContext
    {
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Account> Accounts { get; set; }
    }
}
